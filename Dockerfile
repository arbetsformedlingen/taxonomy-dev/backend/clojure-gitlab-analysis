FROM docker.io/clojure:temurin-17-tools-deps-alpine AS base

WORKDIR /src

RUN apk --no-cache add curl git

# install Babashka 
RUN curl -sLO https://raw.githubusercontent.com/babashka/babashka/master/install
RUN chmod +x install
RUN ./install

COPY bb /src/bb
COPY bb.edn /src/bb.edn

# purpose of following run commands is to cache the clojure dependencies that is downloaded into the image. 
RUN clojure -Sdeps "{:deps {jonase/eastwood {:mvn/version \"1.4.0\"}}}"  -M eastwood.lint; exit 0
RUN clojure -Sdeps "{:deps {jonase/kibit {:mvn/version \"0.1.6\"}}}" -e "(require '[kibit.driver :as k]) (k/external-run [\"src\"] nil)" ; exit 0


# at first entrypoint was set to: ENTRYPOINT ["/usr/local/bin/bb", "-m", "analyse"]
# but it was not apparent how to run this image in the gitlab-ci.yml then. 

