# clojure-gitlab-analysis


to run this tool you must install Babashka or run docker image.

bb -m analyse "path-to-clojure-repo-you-want-to-analyse"
ex: 
`bb -m analyse ../jobtech-taxonomy-api`

this will output a report file `gl-code-quality-report.json` in the same folder that you run the script from

To push new container image after making commit in this repo: 
1. podman login registry.gitlab.com
2. podman build -t registry.gitlab.com/arbetsformedlingen/taxonomy-dev/backend/clojure-gitlab-analysis .
3. podman push registry.gitlab.com/arbetsformedlingen/taxonomy-dev/backend/clojure-gitlab-analysis
