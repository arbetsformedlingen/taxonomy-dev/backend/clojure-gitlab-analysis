At src/clj/jobtech_taxonomy_api/db/concepts.clj:70:
Consider using:
  (nil? include-deprecated)
instead of:
  (= include-deprecated nil)

At src/clj/jobtech_taxonomy_api/db/concepts.clj:73:
Consider using:
  (update query :where conj '(not [?c :concept/deprecated true]))
instead of:
  (-> query (update :where conj '(not [?c :concept/deprecated true])))

At src/clj/jobtech_taxonomy_api/db/concepts.clj:192:
Consider using:
  (pos? (count existing))
instead of:
  (> (count existing) 0)

At src/clj/jobtech_taxonomy_api/db/concepts.clj:195:
Consider using:
  (when result (nth (first (:tx-data result)) 2))
instead of:
  (if result (nth (first (:tx-data result)) 2) nil)

At src/clj/jobtech_taxonomy_api/db/concepts.clj:425:
Consider using:
  (not= id (:concept/id (first p1__515722#)))
instead of:
  (not (= id (:concept/id (first p1__515722#))))

At src/clj/jobtech_taxonomy_api/db/concepts.clj:454:
Consider using:
  (when-not (:result duplicate-exists)
    (db/transact
      (db/get-conn)
      {:tx-data [concept (api-util/user-action-tx user-id comment)]}))
instead of:
  (when (not (:result duplicate-exists))
    (db/transact
      (db/get-conn)
      {:tx-data [concept (api-util/user-action-tx user-id comment)]}))

At src/clj/jobtech_taxonomy_api/db/daynotes.clj:40:
Consider using:
  (cond->
    {:query
     '{:find
       [?tx ?inst ?user-id ?comment ?r ?substitutability_percentage],
       :keys
       [tx
        timestamp
        user-id
        comment
        relation
        substitutability_percentage],
       :in [$],
       :where
       [[?c :concept/id ?input-concept-id]
        (or [?r :relation/concept-1 ?c] [?r :relation/concept-2 ?c])
        (or [?r _ _ ?tx] [?tx :daynote/ref ?r])
        [(get-else $ ?r :relation/substitutability-percentage 0)
         ?substitutability_percentage]
        [(get-else $ ?tx :taxonomy-user/id "@system") ?user-id]
        [(get-else $ ?tx :daynote/comment false) ?comment]
        [?tx :db/txInstant ?inst]]},
     :args [hist-db]}
    concept-id
    (conj-in [:query :in] '?input-concept-id [:args] concept-id)
    from-timestamp
    (conj-in
      [:query :in]
      '?from-inst
      [:query :where]
      '[(<= ?from-inst ?inst)]
      [:args]
      from-timestamp)
    to-timestamp
    (conj-in
      [:query :in]
      '?to-inst
      [:query :where]
      '[(< ?inst ?to-inst)]
      [:args]
      to-timestamp))
instead of:
  (-> {:query
       '{:find
         [?tx ?inst ?user-id ?comment ?r ?substitutability_percentage],
         :keys
         [tx
          timestamp
          user-id
          comment
          relation
          substitutability_percentage],
         :in [$],
         :where
         [[?c :concept/id ?input-concept-id]
          (or [?r :relation/concept-1 ?c] [?r :relation/concept-2 ?c])
          (or [?r _ _ ?tx] [?tx :daynote/ref ?r])
          [(get-else $ ?r :relation/substitutability-percentage 0)
           ?substitutability_percentage]
          [(get-else $ ?tx :taxonomy-user/id "@system") ?user-id]
          [(get-else $ ?tx :daynote/comment false) ?comment]
          [?tx :db/txInstant ?inst]]},
       :args [hist-db]}
   (cond->
     concept-id
     (conj-in [:query :in] '?input-concept-id [:args] concept-id)
     from-timestamp
     (conj-in
       [:query :in]
       '?from-inst
       [:query :where]
       '[(<= ?from-inst ?inst)]
       [:args]
       from-timestamp)
     to-timestamp
     (conj-in
       [:query :in]
       '?to-inst
       [:query :where]
       '[(< ?inst ?to-inst)]
       [:args]
       to-timestamp)))

At src/clj/jobtech_taxonomy_api/db/daynotes.clj:69:
Consider using:
  (db/q
    (build-fetch-relation-txs-query
      hist-db
      concept-id
      from-timestamp
      to-timestamp))
instead of:
  (-> (build-fetch-relation-txs-query
        hist-db
        concept-id
        from-timestamp
        to-timestamp)
   db/q)

At src/clj/jobtech_taxonomy_api/db/graphql/version.clj:43:
Consider using:
  (mapv
    #(p/make-node % :context {:db (db/as-of db (:timestamp %))})
    (sort-by :id))
instead of:
  (->>
    (sort-by :id)
    (mapv #(p/make-node % :context {:db (db/as-of db (:timestamp %))})))

At src/clj/jobtech_taxonomy_api/routes/parameter_util.clj:64:
Consider using:
  (assoc
    (taxonomy/par (relation-types) "Relation type")
    :form
    (relation-types))
instead of:
  (-> (taxonomy/par (relation-types) "Relation type")
   (assoc :form (relation-types)))
