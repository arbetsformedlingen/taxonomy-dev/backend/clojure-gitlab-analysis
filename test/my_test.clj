(ns my-test
  (:require [clojure.test :refer [deftest is testing]]
            [kibit-parser]))

(deftest my-first-test
  (testing "equality works"
    (is (= 1 1))))


(deftest match
  (testing "equality still works"
    (is (= "src/clj/jobtech_taxonomy_api/db/concepts.clj"
           (kibit-parser/get-file-path-kibit "At src/clj/jobtech_taxonomy_api/db/concepts.clj:70:")))))

(deftest get-row
  (testing "equality still works"
    (is (= 195
           (kibit-parser/get-row-num "At src/clj/jobtech_taxonomy_api/db/concepts.clj:195:\nConsider ")))))



(comment 
  
  #_(def kibit-dummy-output (-> (slurp "kibit_output.txt")
                                (split-each-kibit-find)))
  
  (->> (kibit-output->code-climate (slurp "test/kibit_output.txt"))
       (map #(get-in % [:location :lines :begin])))
  
  ,)