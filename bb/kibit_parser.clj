(ns kibit-parser
  (:require [clojure.string :as str]))

(defn split-each-kibit-find [s]
  (str/split s #"\n(?=At\s)"))

(defn get-file-path-kibit [s]
  (re-find #"(?<=At\s)[^:]+" s))

(defn get-row-num [s]
  (-> (re-find #"(?!<:)\d+[^:]" s)
      parse-long))

(defn- output [s]
  (let [[path-info message] (str/split s #"\n" 2)
        file-path (get-file-path-kibit path-info)
        line-num (get-row-num path-info)]
    {:description message
     :check-name "kibit"
     :fingerprint (random-uuid)
     :severity "minor"
     :location {:path file-path
                :lines
                {:begin line-num}}}))

(defn kibit->code-climate [s]
  (->> (split-each-kibit-find s)
       (mapv output)))


(comment

;  [
;  {
;    "description": "'unused' is assigned a value but never used.",
;    "fingerprint": "7815696ecbf1c96e6894b779456d330e",
;    "severity": "minor",
;    "location": {
;      "path": "lib/index.js",
;      "lines": {
;        "begin": 42
;      }
;    }
;  }
;]
  )

