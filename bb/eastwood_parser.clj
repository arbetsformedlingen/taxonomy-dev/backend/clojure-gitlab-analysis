(ns eastwood-parser
  (:require [clojure.string :as str]))

(defn- get-file-path [s]
  (re-find #"[^:]+" s))

(defn- get-row-num [s]
  (-> (re-find #"(?!<:)\d+[^:]" s)
      parse-long))

(defn- output [s]
  (let [[path-info message] (str/split s #"\s" 2)
        file-path (get-file-path path-info)
        line-num (get-row-num path-info)]
    {:description message
     :fingerprint (random-uuid)
     :severity "minor"
     :location {:path file-path
                :lines
                {:begin line-num}}}))

(defn- split-text-entries [s]
  (->> (str/split-lines s)
       (filter (fn [s] (or (str/starts-with? s "src/clj")
                           (str/starts-with? s "test/clj"))))))

(defn eastwood->code-climate [s]
  (->> (split-text-entries s)
       (mapv output)))
