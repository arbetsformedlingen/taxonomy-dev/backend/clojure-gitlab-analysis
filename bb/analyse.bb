#!/usr/bin/env bb
(ns analyse
  (:require [clojure.tools.cli :refer [parse-opts]]
            [babashka.fs :as fs]
            [eastwood-parser]
            [kibit-parser]
            [babashka.process :refer [sh shell process exec]]
            [cheshire.core :as json]))

(def cli-options
  ;; An option with a required argument
  [["-p" "--repo-path=repo_path" "Repofilepath"]
   ["-h" "--help"]])

(defn -main [& _args]
  (let [opts (:options (parse-opts *command-line-args* cli-options))
        _ (println opts)

        repo-path (:repo-path opts)
        ;_ (println (sh "ls /src"))
        ;_ (println (sh (str "ls " repo-path)))
        eastwood-output (-> (sh {:out :string
                                 :dir repo-path
                                 :continue true}
                                "clojure -M:test:eastwood")
                            :out
                            eastwood-parser/eastwood->code-climate)
        kibit-output (-> (sh {:out :string
                              :dir repo-path
                              :continue true}
                             "clojure"
                             "-Sdeps"
                             "{:deps {jonase/kibit {:mvn/version \"0.1.6\"}}}"
                             "-e"
                             "(require '[kibit.driver :as k]) (k/external-run [\"src\"] nil)")
                         :out
                         kibit-parser/kibit->code-climate)
        combined-output (concat eastwood-output kibit-output)
        json-report (json/generate-string combined-output)]
    (spit (str repo-path "/gl-code-quality-report.json") json-report)
    (println (str "report saved under " repo-path))))